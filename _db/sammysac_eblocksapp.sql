-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 03, 2020 at 01:16 AM
-- Server version: 10.1.44-MariaDB-cll-lve
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sammysac_eblocksapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `Categories`
--

CREATE TABLE `Categories` (
  `CategoryId` int(11) NOT NULL,
  `CategoryName` varchar(150) NOT NULL,
  `Description` varchar(255) NOT NULL,
  `Picture` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `eb_roles`
--

CREATE TABLE `eb_roles` (
  `id` int(11) NOT NULL,
  `role_id` varchar(6) NOT NULL,
  `role_description` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eb_roles`
--

INSERT INTO `eb_roles` (`id`, `role_id`, `role_description`, `created_at`, `updated_at`) VALUES
(1, 'adm231', 'Administrator', '2020-05-21 10:12:19', NULL),
(2, 'sup645', 'Supplier', '2020-05-21 10:12:19', NULL),
(3, 'usr059', 'Warehouse User', '2020-05-21 10:12:35', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `eb_users`
--

CREATE TABLE `eb_users` (
  `id` int(11) NOT NULL,
  `username` varchar(150) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email_address` varchar(255) NOT NULL,
  `firstname` varchar(150) NOT NULL,
  `lastname` varchar(150) DEFAULT NULL,
  `activity` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `thatrole` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eb_users`
--

INSERT INTO `eb_users` (`id`, `username`, `password`, `email_address`, `firstname`, `lastname`, `activity`, `created_at`, `updated_at`, `thatrole`) VALUES
(1, 'ebadmin', 'eBAdmin2020', 'ebadmin@sammysa.co.za', 'Eblocks', 'Admin', 1631, '2020-05-21 10:06:21', NULL, 'adm231'),
(2, 'samuser', 'SamUser2020', 'sammy@sammysa.co.za', 'Sammy', 'Phatlane', 1631, '2020-05-21 10:06:21', NULL, 'usr059'),
(3, 'ebmanager', 'eBManager', 'ebmanager@sammysa.co.za', 'eBlocks', 'Manager', 1873, '2020-05-21 10:08:46', NULL, 'sup645'),
(4, 'ebsupplier', 'eBSupplier', 'esupplier@sammysa.co.za', 'eBlocks', 'Supplier', 1631, '2020-05-21 10:08:46', NULL, 'sup645');

-- --------------------------------------------------------

--
-- Table structure for table `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(1);

-- --------------------------------------------------------

--
-- Table structure for table `OrderDetails`
--

CREATE TABLE `OrderDetails` (
  `OrderId` int(11) NOT NULL,
  `ProductId` int(11) NOT NULL,
  `UnitPrice` float NOT NULL,
  `Quantity` int(11) NOT NULL,
  `Discount` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `ProductId` int(11) NOT NULL,
  `ProductName` varchar(150) NOT NULL,
  `SupplierId` int(11) NOT NULL,
  `CategoryId` int(11) NOT NULL,
  `QuantityPerUnit` int(11) NOT NULL,
  `UnitPrice` float NOT NULL,
  `UnitsInStock` int(11) NOT NULL,
  `UnitsOnOrder` int(11) NOT NULL,
  `ReorderLevel` int(11) NOT NULL,
  `Discontinued` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE `suppliers` (
  `id` int(11) NOT NULL,
  `SupplierId` int(10) NOT NULL,
  `CompanyName` varchar(250) NOT NULL,
  `ContactName` varchar(250) NOT NULL,
  `ContactTitle` varchar(5) NOT NULL,
  `Address` varchar(255) NOT NULL,
  `City` varchar(255) NOT NULL,
  `Region` varchar(255) NOT NULL,
  `PostalCode` varchar(8) NOT NULL,
  `Country` varchar(255) NOT NULL,
  `Phone` varchar(15) NOT NULL,
  `Fax` varchar(15) NOT NULL,
  `Homepage` text NOT NULL,
  `activated` bit(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `contact_name` varchar(255) DEFAULT NULL,
  `contact_title` varchar(255) DEFAULT NULL,
  `postal_code` varchar(255) DEFAULT NULL,
  `supplier_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`id`, `SupplierId`, `CompanyName`, `ContactName`, `ContactTitle`, `Address`, `City`, `Region`, `PostalCode`, `Country`, `Phone`, `Fax`, `Homepage`, `activated`, `created_at`, `updated_at`, `company_name`, `contact_name`, `contact_title`, `postal_code`, `supplier_id`) VALUES
(1, 2147483647, 'Sammy Corp', 'Samuel Phatlane', 'Mr', '26 Willowvale Road', 'Blairgworie', 'Gauteng', '2194', 'South Africa', '0798118727', '0861234567', 'http://www.sammysa.co.za', b'1', '2020-05-24 15:04:06', NULL, NULL, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Categories`
--
ALTER TABLE `Categories`
  ADD PRIMARY KEY (`CategoryId`);

--
-- Indexes for table `eb_roles`
--
ALTER TABLE `eb_roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `role_id` (`role_id`);

--
-- Indexes for table `eb_users`
--
ALTER TABLE `eb_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email_address` (`email_address`);

--
-- Indexes for table `OrderDetails`
--
ALTER TABLE `OrderDetails`
  ADD PRIMARY KEY (`OrderId`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`ProductId`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `SupplierId` (`SupplierId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Categories`
--
ALTER TABLE `Categories`
  MODIFY `CategoryId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `eb_roles`
--
ALTER TABLE `eb_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `eb_users`
--
ALTER TABLE `eb_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `OrderDetails`
--
ALTER TABLE `OrderDetails`
  MODIFY `OrderId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `ProductId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
