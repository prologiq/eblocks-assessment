# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Functional Requirement ###
* As a Warehouse manager, I want to filter products based on a category
so that we can easily see which products belong to a supplier

* As a warehouse manager, I want to filter all my products by supplier so
that we can easily see which product belong to a supplier

### Non-Functional Requirement ###
* The product needs to update in real time as products coming into the
  products table

### Overview ###
| Methods  | Urls  |  Actions |
|---|---|---|
|POST|  /api/product | Create new product |
|POST|  /api/category | Create new category |
|POST|  /api/supplier | Create new supplier |
|POST|  /api/order | Create new order |
|GET|   /api/products |  Retrieve Products |
|GET|   /api/categories |  Retrieve Categories |
|GET|   /api/suppliers |  Retrieve Suppliers |
|GET|   /api/orders |  Retrieve Orders |
|GET|   /api/products?category=:id |  Retrieve Products as per category  |
|GET|   /api/orders/user/:id |  Retrieve Order Per User |
|GET|   /api/orders/:id |  Retrieve Order Per ID |
|PUT|   /api/products/:id|  Update Product By Id|
|PUT|   /api/categories/:id |  Update Category By Id|
|PUT|   /api/suppliers/:id|  Update Supplier By Id |
|PUT|   /api/orders/:id|  Update Order By Id |
|DELETE|/api/product/:id|  Delete Order By Id |

### Spring API ###
Located Under the `warehouse-api` directory. The back-end server uses Spring Boot with Spring Web MVC for REST Controller and Spring Data JPA for interacting with MySQL database. Note that the db is hosted remotely, but will include it in the repo in case you don't have access. Details have been listed below:
#### Technology ####
* Java 8
* Spring Boot 2.3.0
* Database: MySql
* Maven: 3.6.3

#### Remote Database Details ####

* URL: 164.160.91.22
* Username: sammysac_thatdevguy
* Password: v8vNgQaL6L5VpHUX
* Database: sammysac_eblocksapp

* Local DB is also available in the `_db` directory with the basic structure

### Front-end
* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner: Samuel Phatlane(via mail to sammy@sammysa.co.za or phatlanems@gmail.com)
