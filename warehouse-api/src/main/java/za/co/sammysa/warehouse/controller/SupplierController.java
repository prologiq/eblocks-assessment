package za.co.sammysa.warehouse.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;

import za.co.sammysa.warehouse.repository.SupplierRepository;
import za.co.sammysa.warehouse.model.Supplier;

/**
 * @author samuelphatlane
 * RestController
 * has mapping methods for RESTful requests
 *
 */

@CrossOrigin(origins = "http://localhost:8081")
@RestController
@RequestMapping("/api")
public class SupplierController {

    @Autowired
    SupplierRepository supplierRepository;
    
    /*
     * Retrieve all suppliers
     */
    @GetMapping("/suppliers")
    public ResponseEntity<List<Supplier>> getAllSuppliers(@RequestParam(required = false) String CompanyName){
        try {
        	List<Supplier> suppliers = new ArrayList<Supplier>();
        	
        	if(CompanyName == null)
        		supplierRepository.findAll().forEach(suppliers::add);
        	else
        		supplierRepository.findByCompanyNameContaining(CompanyName).forEach(suppliers::add);
        	
        	if(suppliers.isEmpty()) {
        		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        	}
        	
        	return new ResponseEntity<>(suppliers, HttpStatus.OK);
        }catch(Exception e) {
        	return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    /*
     * Retrieve Supplier by ID
     */
    @GetMapping("/suppliers/{id}")
    public ResponseEntity<Supplier> getSupplierById(@PathVariable("SupplierId") long SupplierId){
    	Optional<Supplier> supplierData = supplierRepository.findById(SupplierId);
    	
    	if(supplierData.isPresent())
    	{
    		return new ResponseEntity<>(supplierData.get(), HttpStatus.OK);
    	}else {
    		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    	}
    }
    
    /*
     * Create Supplier
     */
    @PostMapping("/supplier")
    public ResponseEntity<Supplier> createSupplier(@RequestBody Supplier supplier)
    {
    	try {
    		Supplier _supplier = supplierRepository
    				.save(new Supplier(supplier.getId(), supplier.getSupplierId(), 
    						supplier.getCompanyName(), supplier.getContactName(), 
    						supplier.getContactTitle(), supplier.getAddress(), 
    						supplier.getCity(), supplier.getRegion(), 
    						supplier.getPostalCode(), supplier.getCountry(), 
    						supplier.getPhone(), supplier.getFax(), 
    						supplier.getHomepage(), true));
    		return new ResponseEntity<>(_supplier, HttpStatus.CREATED);
    	} catch (Exception e) {
    		return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
    	}
    }
    
    /*
     * Update Supplier By ID
     */
    @PutMapping("/supplier/{id}")
    public ResponseEntity<Supplier> updateSupplier(@PathVariable("SupplierId") long SupplierId, @RequestBody Supplier supplier)
    {
    	Optional<Supplier> supplierData = supplierRepository.findById(SupplierId);
    	
    	if(supplierData.isPresent())
    	{
    		Supplier _supplier = supplierData.get();
    		_supplier.setCompanyName(supplier.getCompanyName());
    		_supplier.setContactName(supplier.getContactName());
    		_supplier.setContactTitle(supplier.getContactTitle());
            _supplier.setAddress(supplier.getAddress());
            _supplier.setCity(supplier.getCity());
            _supplier.setRegion(supplier.getRegion());
            _supplier.setPostalCode(supplier.getPostalCode());
            _supplier.setCountry(supplier.getCountry());
            _supplier.setPhone(supplier.getPhone());
            _supplier.setFax(supplier.getFax());
            _supplier.setHomepage(supplier.getHomepage());
    		return new ResponseEntity<>(supplierRepository.save(_supplier), HttpStatus.OK);
    	}else {
    		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    	}
    }
    
    /*
     * Delete Supplier by ID
     */
    @DeleteMapping("/supplier/{id}")
    public ResponseEntity<HttpStatus> deleteSupplier(@PathVariable("SupplierId") long SupplierId){
    	try {
    		supplierRepository.deleteById(SupplierId);
    		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    	}catch(Exception e) {
    		return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
    	}
    }

    /*
    * Delete all suppliers
     */
	@DeleteMapping("/suppliers")
	public ResponseEntity<HttpStatus> deleteAllSuppliers(){
		try{
			supplierRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}catch(Exception e){
			return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
		}
	}

	/*
	*	Find Active Suppliers
	 */
	@GetMapping("/suppliers/active")
	public ResponseEntity<List<Supplier>> findByActivity(){
		try{
			List<Supplier> suppliersActive = supplierRepository.findByActivated(true);

			if(suppliersActive.isEmpty()){
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<>(suppliersActive, HttpStatus.OK);
		}catch(Exception e){
			return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
		}
	}


}
