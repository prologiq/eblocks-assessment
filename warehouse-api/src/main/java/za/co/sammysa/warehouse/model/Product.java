package za.co.sammysa.warehouse.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "products")
public class Product {

    @Id
    @GeneratedValue
    private long ProductId;

    @Column(name = "ProductName")
    private String ProductName;
    
    @Column(name = "SupplierId")
    private long SupplierId;

    @Column(name = "CategoryId")
    private long CategoryId;

    @Column(name = "QuantityPerUnit")
    private long QuantityPerUnit;

    @Column(name = "UnitPrice")
    private float UnitPrice;

    @Column(name = "UnitsInStock")
    private long UnitsInStock;

    @Column(name = "ReorderLevel")
    private long ReorderLevel;

    @Column(name = "Discontinued")
    private long Discontinued;

    

    public long getProductId() {
        return ProductId;
    }

    public void setProductId(long productId) {
        ProductId = productId;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public long getSupplierId() {
        return SupplierId;
    }

    public void setSupplierId(long supplierId) {
        SupplierId = supplierId;
    }

    public long getCategoryId() {
        return CategoryId;
    }

    public void setCategoryId(long categoryId) {
        CategoryId = categoryId;
    }

    public long getQuantityPerUnit() {
        return QuantityPerUnit;
    }

    public void setQuantityPerUnit(long quantityPerUnit) {
        QuantityPerUnit = quantityPerUnit;
    }

    public float getUnitPrice() {
        return UnitPrice;
    }

    public void setUnitPrice(float unitPrice) {
        UnitPrice = unitPrice;
    }

    public long getUnitsInStock() {
        return UnitsInStock;
    }

    public void setUnitsInStock(long unitsInStock) {
        UnitsInStock = unitsInStock;
    }

    public long getReorderLevel() {
        return ReorderLevel;
    }

    public void setReorderLevel(long reorderLevel) {
        ReorderLevel = reorderLevel;
    }

    public long getDiscontinued() {
        return Discontinued;
    }

    public void setDiscontinued(long discontinued) {
        Discontinued = discontinued;
    }

    public Product(){

    }

    public Product(long productId, String productName, long supplierId, long categoryId, long quantityPerUnit,
            float unitPrice, long unitsInStock, long reorderLevel, long discontinued) {
        ProductId = productId;
        ProductName = productName;
        SupplierId = supplierId;
        CategoryId = categoryId;
        QuantityPerUnit = quantityPerUnit;
        UnitPrice = unitPrice;
        UnitsInStock = unitsInStock;
        ReorderLevel = reorderLevel;
        Discontinued = discontinued;
    }

    @Override
	public String toString(){
        return  "Product Details: [id: " +  ProductId 
                + "ProductName: " + ProductName 
                + "SupplierId: " +  SupplierId 
                + "CategoryId: " + CategoryId 
                + "QuantityPerUnit: " + QuantityPerUnit 
                + "UnitPrice: " + UnitPrice 
                + "UnitsInStock: " + UnitsInStock 
                + "ReorderLevel: " + ReorderLevel 
                + "Discount: " + Discontinued 
                + "]";
	}
    
}