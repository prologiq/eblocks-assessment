/**
 * 
 */
package za.co.sammysa.warehouse.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

import za.co.sammysa.warehouse.model.Supplier;

/**
 * @author samuelphatlane
 * Interface that extends JpaRepository for CRUD Methods
 * Autowired to the SupplierController
 *
 */
public interface SupplierRepository extends JpaRepository<Supplier, Long>{
    List<Supplier> findByActivated(boolean activated);
    List<Supplier> findByCompanyNameContaining(String CompanyName);
}
