package za.co.sammysa.warehouse.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import za.co.sammysa.warehouse.repository.CategoryRepository;
import za.co.sammysa.warehouse.model.Category;

@CrossOrigin(origins = "http://localhost:8081")
@RestController
@RequestMapping("/api")
public class CategoryController {

    @Autowired
    CategoryRepository categoryRepository;

    @GetMapping("/categories")
    public ResponseEntity<List<Category>> getAllCategories(@RequestParam(required = false) String CategoryName){
        try {
        	List<Category> categories = new ArrayList<Category>();
        	
        	if(CategoryName == null)
                categoryRepository.findAll().forEach(categories::add);
        	else
                categoryRepository.findByCategoryNameContaining(CategoryName).forEach(categories::add);
        	
        	if(categories.isEmpty()) {
        		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        	}
        	
        	return new ResponseEntity<>(categories, HttpStatus.OK);
        }catch(Exception e) {
        	return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PostMapping("/category")
    public ResponseEntity<Category> createCategory(@RequestBody Category category)
    {
        try{
            Category _category = categoryRepository.save(new Category(category.getCategoryId(), category.getCategoryName(),
                                category.getDescription(), category.getPicture()));
            return new ResponseEntity<>(_category, HttpStatus.CREATED);
        }catch(Exception e)
        {
            return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
        }
    }
}