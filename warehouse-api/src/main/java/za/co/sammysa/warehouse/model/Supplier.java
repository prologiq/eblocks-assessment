package za.co.sammysa.warehouse.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "suppliers")
public class Supplier {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(name = "SupplierId")
	private String SupplierId;

	@Column(name = "CompanyName")
	private String CompanyName;

	@Column(name = "ContactName")
	private String ContactName;

	@Column(name = "ContactTitle")
	private String ContactTitle;

	@Column(name = "Address")
	private String Address;

	@Column(name = "City")
	private String City;

	@Column(name = "Region")
	private String Region;

	@Column(name = "PostalCode")
	private String PostalCode;

	@Column(name = "Country")
	private String Country;

	@Column(name = "Phone")
	private String Phone;

	@Column(name = "Fax")
	private String Fax;

	@Column(name = "Homepage")
	private String Homepage;

	@Column(name = "activated")
	private boolean activated;

	public Supplier(){

	}

	public Supplier(long id, String supplierId, String companyName,
					String contactName, String contactTitle,
					String address, String city, String region,
					String postalCode, String country, String phone,
					String fax, String homepage, boolean activated) {
		this.id = id;
		this.SupplierId = supplierId;
		this.CompanyName = companyName;
		this.ContactName = contactName;
		this.ContactTitle = contactTitle;
		this.Address = address;
		this.City = city;
		this.Region = region;
		this.PostalCode = postalCode;
		this.Country = country;
		this.Phone = phone;
		this.Fax = fax;
		this.Homepage = homepage;
		this.activated = activated;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getSupplierId() {
		return SupplierId;
	}

	public boolean isActivated() {
		return activated;
	}

	public void setActivated(boolean activated) {
		this.activated = activated;
	}

	public void setSupplierId(String supplierId) {
		SupplierId = supplierId;
	}

	public String getCompanyName() {
		return CompanyName;
	}

	public void setCompanyName(String companyName) {
		CompanyName = companyName;
	}

	public String getContactName() {
		return ContactName;
	}

	public void setContactName(String contactName) {
		ContactName = contactName;
	}

	public String getContactTitle() {
		return ContactTitle;
	}

	public void setContactTitle(String contactTitle) {
		ContactTitle = contactTitle;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}

	public String getCity() {
		return City;
	}

	public void setCity(String city) {
		City = city;
	}

	public String getRegion() {
		return Region;
	}

	public void setRegion(String region) {
		Region = region;
	}

	public String getPostalCode() {
		return PostalCode;
	}

	public void setPostalCode(String postalCode) {
		PostalCode = postalCode;
	}

	public String getCountry() {
		return Country;
	}

	public void setCountry(String country) {
		Country = country;
	}

	public String getPhone() {
		return Phone;
	}

	public void setPhone(String phone) {
		Phone = phone;
	}

	public String getFax() {
		return Fax;
	}

	public void setFax(String fax) {
		Fax = fax;
	}

	public String getHomepage() {
		return Homepage;
	}

	public void setHomepage(String homepage) {
		Homepage = homepage;
	}

	@Override
	public String toString(){
		return "Supplier: [id: " + id + ", SupplierId: " + SupplierId + ", CompanyName: " + CompanyName + ", ContactName: " + ContactName + ", ContactTitle: " + ContactTitle + ", Address: " + Address + ", City: " + City + ", Region: " + Region + ", PostalCode: " + PostalCode + ", Country: " + Country + ", Phone: " + Phone + ", Fax: " + Fax + ", Homepage: " + Homepage + "]";
	}
}
