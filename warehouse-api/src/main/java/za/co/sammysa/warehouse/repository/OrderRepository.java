package za.co.sammysa.warehouse.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import za.co.sammysa.warehouse.model.OrderDetails;

public interface OrderRepository extends JpaRepository<OrderDetails, Long>{
    List<OrderDetails> findByOrderNumber(long OrderId); // Currently we can only search via the Order Number
}