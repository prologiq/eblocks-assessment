package za.co.sammysa.warehouse.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "OrderDetails")
public class OrderDetails {
    
    @Id
    @GeneratedValue
    private long OrderId;

    @Column(name = "ProductId")
    private long ProductId;

    @Column(name = "UnitPrice")
    private float UnitPrice;

    @Column(name = "Quantity")
    private long Quantity;

    @Column(name = "Discount")
    private boolean Discount;

    public OrderDetails(){

    }

    public OrderDetails(long orderId, long productId, float unitPrice, long quantity, boolean discount) {
        OrderId = orderId;
        ProductId = productId;
        UnitPrice = unitPrice;
        Quantity = quantity;
        Discount = discount;
    }

    public long getOrderId() {
        return OrderId;
    }

    public void setOrderId(long orderId) {
        OrderId = orderId;
    }

    public long getProductId() {
        return ProductId;
    }

    public void setProductId(long productId) {
        ProductId = productId;
    }

    public float getUnitPrice() {
        return UnitPrice;
    }

    public void setUnitPrice(float unitPrice) {
        UnitPrice = unitPrice;
    }

    public long getQuantity() {
        return Quantity;
    }

    public void setQuantity(long quantity) {
        Quantity = quantity;
    }

    public boolean isDiscount() {
        return Discount;
    }

    public void setDiscount(boolean discount) {
        Discount = discount;
    }

    @Override
	public String toString(){
        return "Order Details: [id: " +  OrderId + " Product Id: " + ProductId + " Unit Price: " + 
        UnitPrice + "Unit Quantity: " + Quantity + "Discounted: " + Discount + "]";
	}

    
}