package za.co.sammysa.warehouse.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Categories")
public class Category {


    @Id
    @GeneratedValue
    private long CategoryId;

    @Column(name = "CategoryName")
    private String CategoryName;

    @Column(name = "Description")
    private String Description;

    @Column(name = "Picture")
    private String Picture;

    public Category(){

    }

    public Category(long categoryId, String categoryName, String description, String picture) {
        CategoryId = categoryId;
        CategoryName = categoryName;
        Description = description;
        Picture = picture;
    }

    public long getCategoryId() {
        return CategoryId;
    }

    public void setCategoryId(long categoryId) {
        CategoryId = categoryId;
    }

    public String getCategoryName() {
        return CategoryName;
    }

    public void setCategoryName(String categoryName) {
        CategoryName = categoryName;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getPicture() {
        return Picture;
    }

    public void setPicture(String picture) {
        Picture = picture;
    }

    @Override
	public String toString(){
        return "Category Details: [id: " +  CategoryId + " Category Name: " + CategoryName + " Description: " + 
        Description + "Picture: " + Picture + "]";
	}
    
}