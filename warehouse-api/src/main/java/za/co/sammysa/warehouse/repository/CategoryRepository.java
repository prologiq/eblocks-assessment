package za.co.sammysa.warehouse.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

import za.co.sammysa.warehouse.model.Category;


public interface CategoryRepository extends JpaRepository<Category, Long> {
    List<Category> findByDiscounted(boolean discount);
    List<Category> findByCategoryNameContaining(String CategoryName);
    
}